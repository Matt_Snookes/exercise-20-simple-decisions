﻿using System;

namespace exercise_20_simple_decisions
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
 /* 
            var num1=0;
            var num2=0;
            
            

            Console.WriteLine("Enter first number");
            num1= int.Parse (Console.ReadLine());
            Console.WriteLine("Enter second number");
            num2=int.Parse(Console.ReadLine());
            if (num1==num2)
            {
                Console.WriteLine("These numbers are equal");
            }
            else
            {
                Console.WriteLine("These numbers are not equal");
            }

          */
           var password="password"; 
            
            do
            {
            Console.WriteLine($"The current password is {password} Please create a new password with 8 letters?");
            password =Console.ReadLine();
            if (password.Length <8 )
                 {
                 Console.WriteLine("Password to short");
                 }
            else if ( password.Length >8  )
                 {
                 Console.WriteLine("password to long");
                 }
            else if  (password=="password")
                 {
                 System.Console.WriteLine("That is the existing password, Please create a new password that contains 8 letters");
                 }
            }
            while (password.Length <8 && password !="password");  
            password = password.Replace("password", "");
            System.Console.WriteLine($"the new password is {password}");
            
    
        
    
/* 
            System.Console.WriteLine("enter a password");
            password= Console.ReadLine();
            
            password = password.Replace("password", "");
            System.Console.WriteLine($"the new password is {password}");

            */

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
            
        }
    }
}
